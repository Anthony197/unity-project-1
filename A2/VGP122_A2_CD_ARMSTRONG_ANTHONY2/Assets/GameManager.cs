﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public Text scoreText;// variable for displaying the score text
    int _score;//handles score
    bool muted = false;

    static GameManager _instance = null;
    //^ keeps track of the game mangager to insure only one is made/acive
    public GameObject playerPrefab;
    //^ allows me to set the prefab for the players character in the inspector


    // Use this for initialization
    void Start () {

        if (!playerPrefab)
        {
            Debug.LogWarning("Player Prefab not set in inspector!");
        }

        //check if GameManager already exists
        if (instance)
        {
            // destroy the second copy if one exists
            DestroyImmediate(gameObject);
        }
        else
        {
            //assign to _instance
            instance = this;
            //stops game manager from being destroyed when switching levels
            DontDestroyOnLoad(this);
        }

        //sets intial score on start of game manager
        score = 0;


    }
	
	// Update is called once per frame
	void Update () {
        if (muted == false)
        {
            AudioListener.pause = false;
            Debug.Log("game is unmuted");
        }
        else
        {
            AudioListener.pause = true;
            Debug.Log("game is muted");
        }
		
	}

    public void Mute()
    {
       if (muted == false)
        {
            muted = true;
        }
        else
        {
            muted = false;
        }
    }

    public void StartGame()
    {
        //loads level1
        SceneManager.LoadScene("Scene");
    }

    public void QuitGame()
    {
        Debug.Log("quit game");
        Application.Quit();
    }
    public void Credits()
    {
        SceneManager.LoadScene("Credits");
    }

    public static GameManager instance
    {
        get { return _instance; }   // or get;
        set { _instance = value; }  // or set;
    }

    public int score
    {
        get { return _score; }
        set
        {
            _score = value;
            if (scoreText)
            {
                scoreText.text = "Score: " + score;
            }
        }
    }
}
